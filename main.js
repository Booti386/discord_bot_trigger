if(!console)
	console = {};
if(!console.log)
	console.log = function(a){};

function pingDiscord(token) {
	let ws = new WebSocket('wss://gateway.discord.gg?v=6&encoding=json');
	ws.OP_DISPATCH = 0;
	ws.OP_IDENT = 2;

	ws.got_ready_evt = false;

	ws.sendjson = function (a) {
		a = JSON.stringify(a);
		console.log('--ws_send(' + a + ')');
		return this.send(a);
	};

	ws.sendop = function (op, a) {
		return this.sendjson({
			'op': op,
			'd': a
		});
	};

	ws.onopen = function (ev) {
		console.log('--ws_open--');

		this.sendop(this.OP_IDENT, {
			'token': 'Bot ' + token,
			'properties': {}
		});
	};

	ws.onmessage = function (ev) {
		console.log('--ws_msg: ' + ev.data);

		data = JSON.parse(ev.data);
		switch (data.op)
		{
			case this.OP_DISPATCH:
				switch (data.t)
				{
					case "READY":
						this.got_ready_evt = true;
						this.close();
						break;
				}
				break;
		}
	};

	ws.onerror = function (ev) {
		console.log('--ws_err--');
	};

	ws.onclose = function (ev) {
		console.log('--ws_close code: ' + ev.code + '--');

		if (this.got_ready_evt)
			alert("Got READY event, it probably worked!");
		else
			alert("Didn't get READY event, please check your token!");
	};
}
